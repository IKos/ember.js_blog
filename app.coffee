###
Module dependencies.
###
express = require("express")
http = require("http")
path = require("path")
bodyParser = require("body-parser")
mongoose   = require("mongoose")
config = require("./lib/config")

app = express()

mongoose.connect config.get("mongoose:uri")
require("./models/post.model.coffee")
Post = mongoose.model 'posts'

#middleware for CORS
allowCrossDomain = (req, res, next) ->
  res.header "Access-Control-Allow-Origin", "*"
  res.header "Access-Control-Allow-Methods", "GET,PUT,POST,DELETE"
  res.header "Access-Control-Allow-Headers", "Content-Type"
  next()
  return

app.use bodyParser()
app.use allowCrossDomain

mongoose.set 'debug', yes

#app.use app.router
app.use require("stylus").middleware(__dirname + "/client/public")
console.log __dirname
app.use express.static(path.join(__dirname, "/client/public"))
app.use require('connect-assets')(buildDir: "/client/public")
#app.use express.errorHandler()

# app.use((req,res,next)->
#   req.db = db
#   next()
#   )

# ROUTES FOR OUR API
# =============================================================================
router = express.Router()

# middleware to use for all requests
router.use (req, res, next) ->

  # do logging
  console.log "I just log it!"
  next() # make sure we go to the next routes and don't stop here
  return

# test route to make sure everything is working (accessed at GET http://localhost:3333/api)
router.get "/", (req, res) ->
  res.json message: "API for Post here."
  return

#************************************************************************************************
app.post '/posts', (req, res) ->
  console.log "post /posts"
  post = new Post()
  post.index = req.body.post.index
  post.title = req.body.post.title
  post.author = req.body.post.author
  post.intro = req.body.post.intro
  post.extended = req.body.post.extended
  post.publishedAt = new Date
  post.save (err) ->
    console.log err if err
    return res.send err  if err
    res.json post: post

app.get '/posts', (req, res) ->
  console.log "get /posts"
  page = parseInt(req.query.page)
  perPage = parseInt(req.query.perPage)
  skip = (page - 1) * perPage

  sort = {}
  limit = null
  if req.query.sort?
    sort[req.query.sort] = req.query.order
  if req.query.limit?
    limit = req.query.limit

  if sort and limit
    Post.find().limit(limit).sort(sort).exec (err, posts)->
      return res.json 500, error: err  if err
      res.json posts: posts
  else
    Post.count (err, cnt)->
      return res.json 500, error: err  if err
      Post.find().limit(perPage).skip(skip).exec (err, posts) ->
        return res.json 500, error: err  if err
        res.json 
          posts: posts
          pagination:
            page: page
            perPage: perPage
            total: cnt

#************************************************************************************************
app.get "/posts/:post_id", (req, res) ->
  Post.findById req.params.post_id, (err, post) ->
      console.log "get /posts/" + req.params.post_id
      if !post
        res.statusCode = 404
        return res.send error: 'Not found'
      if !err
        return res.send post: post
      res.statusCode = 500
      log.error 'Internal error(%d): %s',res.statusCode,err.message
      return res.send error : 'Server error'  
  return

app.put "/posts/:post_id", (req, res) ->
  console.log "put /posts/" + req.params.post_id
  Post.findById req.params.post_id, (err, post) ->
    res.send err  if err
    post.title = req.body.post.title
    post.author = req.body.post.author
    post.intro = req.body.post.intro
    post.extended = req.body.post.extended
    post.publishedAt = req.body.post.publishedAt
    post.save (err, post) ->
      res.send err  if err
      res.json post: post
      return
    return
  return

app.delete "/posts/:post_id", (req, res) ->
  console.log "delete /posts/" + req.params.post_id
  Post.findByIdAndRemove req.params.post_id, (err, post) ->
    return res.send 500, err  if err
    res.json 200, post: post



# REGISTER OUR ROUTES -------------------------------
# all of our routes will be prefixed with /api
app.use "/api", router

# START THE SERVER  port = process.env.PORT or 3333
# =============================================================================
app.listen config.get('port')
console.log 'Express server listening on port ' + config.get 'port'