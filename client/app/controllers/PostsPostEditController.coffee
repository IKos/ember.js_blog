App = require 'app'

module.exports = App.PostsViewEditController = Ember.ObjectController.extend
  actions: 
    save: () ->
      @get('model').save().then (post)=>
        @transitionToRoute 'posts.view', post

    cancel: ()->
      @get('model').rollback()
      @transitionToRoute 'posts'