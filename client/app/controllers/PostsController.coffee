App = require 'app'

module.exports = App.PostsIndexController = Ember.ArrayController.extend
  content: []

  actions: 
    goPage: (page_id)->
      @transitionToRoute 'posts', 
        queryParams:
          page: page_id

  getCountPage: (-> 
      pag = @store.metadataFor(@get 'model.type').pagination
      countPage = Math.floor(pag.total/pag.perPage)
      if pag.total%pag.perPage!=0
        ++countPage
      arr = []
      --countPage
      for i in [0..countPage]
        arr[i] = i + 1
      
      return arr
    ).property 'page'