App = require 'app'

module.exports = App.PostsCreateController = Ember.ObjectController.extend
  actions: 
    save: () ->
      model = @get 'model'
      model.save().then (post)=>
        @transitionToRoute 'index'

    cancel: ()->
      @get('model').deleteRecord()
      @transitionToRoute 'index'