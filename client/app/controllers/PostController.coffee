App = require 'app'

module.exports = App.PostsViewIndexController = Ember.ObjectController.extend
  actions:
    delete: ->
      post = @get 'model'
      post.deleteRecord()
      post.save().then ()=>
        console.log 'Post deleted'
        @transitionToRoute('posts')

  modelChanged: (->
    console.log @get 'model'
  ).observes 'content'