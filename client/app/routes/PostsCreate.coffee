App = require 'app'

module.exports = App.PostsCreateRoute = Ember.Route.extend
  model: (params)->
    @store.createRecord 'post',
      title: 'New post', author: 'I. Am. Author', publishedAt: new Date, intro: 'This is little intro', extended: 'This is body of the post'

