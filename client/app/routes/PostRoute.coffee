App = require 'app'

module.exports = App.PostsViewRoute = Ember.Route.extend
  model: (params)->
    @_super arguments...
    console.log 'PostsViewRoute model', params
    @store.find('post', params.post_id)
