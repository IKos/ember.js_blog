App = require 'app'

module.exports = App.PostsRoute = Ember.Route.extend
  model: (params)->
    queryParams = params.queryParams or {}
    searchParams =
      page: queryParams.page or 1
      perPage: queryParams.perPage or 10
    @store.find 'post', searchParams

  setupController: (controller, model)->
    @_super arguments...
    @store.find('post',
      sort: 'publishedAt'
      order: -1
      limit: 5
    ).then (posts)->
      controller.set 'recentPosts', posts

  actions:
    queryParamsDidChange: ()->
      @refresh()