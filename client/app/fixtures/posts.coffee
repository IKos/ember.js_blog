App = require 'app'

module.exports = App.Post.FIXTURES = [
    id: 1
    # index: 1
    title: 'one'
    author: 'ivan'
    publishedAt: new Date('2014-05-11')
    intro: 'nope'
    extended: '**bla***bla*bla'
,
    id: 2
    # index: 2
    title: 'title'
    author: 'author'
    publishedAt: new Date('2014-03-17')
    intro: 'intro'
    extended: 'extended'
,
    id: 3
    # index: 3
    title: 'title'
    author: 'author'
    publishedAt: new Date('2014-03-17')
    intro: 'intro'
    extended: 'extended'
]
