App = require 'app'
# App.ApplicationStore = DS.Store.extend()

App.ApplicationAdapter = DS.RESTAdapter.extend
  host: 'http://localhost:3333'
  # namespace: @inheritDoc
#   serializer: App.ApplicationSerializer

App.ApplicationSerializer = DS.RESTSerializer.extend
  primaryKey: '_id'

  extractMeta: (store, type, payload)->
    if payload and payload.pagination
      store.metaForType type,
        pagination: payload.pagination
      delete payload.pagination
    @_super arguments...