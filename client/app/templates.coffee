# this file is auto-completed if you are using generators.
# Example:
# `scaffolt template user/index`
require 'templates/application'
require 'templates/posts'
require 'templates/posts/index'
require 'templates/posts/view/index'
require 'templates/posts/view/edit'
require 'templates/posts/create'
require 'templates/posts/_form'
