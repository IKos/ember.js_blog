App = require 'app'


App.Router.map ->
  # put your routes here
  @resource 'posts', {queryParams: ['page', 'perPage']}, ()->
    @route 'create'
    @resource 'posts.view', {path: ':post_id'}, ()->
      @route 'edit', {path: 'edit'}