mongoose     = require('mongoose')
Schema       = mongoose.Schema

post   = new Schema
  title: String
  author: String
  intro: String
  extended: String
  publishedAt: Date

module.exports = mongoose.model 'posts', post
